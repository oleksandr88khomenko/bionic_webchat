package com.springapp.mvc.datastore;

/**
 * Created by oleksandr_khomenko on 16.04.14.
 */

import com.springapp.mvc.RupsDate;
import com.springapp.mvc.hibernateBeans.Message;
import com.springapp.mvc.service.MessageDatastoreService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * @author : Rupesh Shirude
 * @version : 1.0 15-3-2013
 * @since : 1.0
 */
@Service
public class MessageDataStore implements MessageDatastoreService {

    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Override
    public void sendMessageToThisUser(String receiverName, String message,
                                      String senderName) {
        Message message2 = new Message();
        message2.setMessage(message);
        message2.setReceiverName(receiverName);
        message2.setSenderName(senderName);
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        RupsDate date2 = new RupsDate();
        date2.setDay(date.getDay());
        date2.setMonth(date.getMonth()+1);
        date2.setYear(date.getYear());
        date2.setMin(date.getMinutes());
        date2.setSec(date.getSeconds());
        date2.setHour(date.getHours());
        message2.setTime(date2);
        hibernateTemplate.save(message2);
    }

    @Override
    public List getMyLatestMessages(String loggedUserName) {
        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(Message.class)
                .add(Restrictions.like("receiverName", loggedUserName))
                .addOrder(org.hibernate.criterion.Order.desc("id"))
                .setMaxResults(5);
        List<Message> messageList = criteria.list();
        return messageList;
    }

    @Override
    public List getMyPrevMessages(String loggedUserName, String minVal) {
        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(Message.class)
                .add(Restrictions.like("receiverName", loggedUserName))
                .add(Restrictions.lt("id", Integer.parseInt(minVal)))
                .addOrder(org.hibernate.criterion.Order.desc("id"))
                .setMaxResults(5);
        List<Message> messageList = criteria.list();
        return messageList;
    }

    @Override
    public List getMyNextMessages(String loggedUserName, String maxVal) {
        System.out.println("maxVal "+maxVal);
        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(Message.class)
                .add(Restrictions.like("receiverName", loggedUserName))
                .add(Restrictions.gt("id", Integer.parseInt(maxVal)))
                .addOrder(org.hibernate.criterion.Order.desc("id"))
                .setMaxResults(5);
        List<Message> messageList = criteria.list();
        return messageList;
    }

}
