package com.springapp.mvc.datastore;

/**
 * Created by oleksandr_khomenko on 16.04.14.
 */

import com.springapp.mvc.hibernateBeans.OnlineUsers;
import com.springapp.mvc.hibernateBeans.User;
import com.springapp.mvc.service.UserDatastoreService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserDatastore implements UserDatastoreService {

    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Override
    public void createUserByHibernate(String name, String password) {
        User user = new User();
        user.setUserName(name);
        user.setPassWord(password);
        hibernateTemplate.save(user);
    }

    @Override
    public User loginByUsernameAndPassword(String username, String password) {

        Criteria criteria = hibernateTemplate.getSessionFactory().openSession()
                .createCriteria(User.class)
                .add(Restrictions.eq("userName", username))
                .add(Restrictions.eq("passWord", password));
        return (User) criteria.list().get(0);
    }

    @Override
    public List getAllUsers(String loggedUserName) {

        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(User.class)
                .add(Restrictions.ne("userName", loggedUserName));
        return criteria.list();

    }

    @Override
    public List getOnlineUsers(String loggedUserName) {

        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(OnlineUsers.class)
                .add(Restrictions.eq("liveStatus", 1))
                .add(Restrictions.ne("userId",loggedUserName));
        return criteria.list();

    }

    @Override
    public void changeMyOnlineStatus(String loggedUserName,int status) {
        Criteria criteria = hibernateTemplate.getSessionFactory()
                .openSession()
                .createCriteria(OnlineUsers.class)
                .add(Restrictions.eq("userId", loggedUserName));
        if(criteria.list().size() == 0){
            OnlineUsers onlineUsers = new OnlineUsers();
            onlineUsers.setLiveStatus(status);
            onlineUsers.setUserId(loggedUserName);
            hibernateTemplate.save(onlineUsers);
        }else{
            OnlineUsers onlineUsers = (OnlineUsers) criteria.list().get(0);
            onlineUsers.setLiveStatus(status);
            hibernateTemplate.update(onlineUsers);
        }
    }
}