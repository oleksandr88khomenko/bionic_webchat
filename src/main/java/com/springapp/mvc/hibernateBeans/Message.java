package com.springapp.mvc.hibernateBeans;

/**
 * Created by oleksandr_khomenko on 16.04.14.
 */
import com.springapp.mvc.RupsDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="MESSAGE")
public class Message {
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @Column(name="SENDER")
    private String senderName ;

    @Column(name="RECEIVER")
    private String receiverName ;

    @Column(name="MESSAGE")
    private String message ;

    @Column(name="TIME")
    private RupsDate time ;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getSenderName() {
        return senderName;
    }
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
    public String getReceiverName() {
        return receiverName;
    }
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public RupsDate getTime() {
        return time;
    }
    public void setTime(RupsDate time) {
        this.time = time;
    }

}