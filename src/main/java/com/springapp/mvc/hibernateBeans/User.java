package com.springapp.mvc.hibernateBeans;

/**
 * Created by oleksandr_khomenko on 16.04.14.
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class User {

    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @Column(name="USERNAME")
    private String userName ;

    @Column(name="PASSWORD")
    private String passWord ;


    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassWord() {
        return passWord;
    }
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}